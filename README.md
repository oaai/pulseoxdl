# About

**pulseoxdl - pulse oximetry downloader**

Pulse oximeter device (from hereon -- device):

* Contec CMS50E with USB HID.
* Hardware: 2.0.0.
* Firmware: 2.0.2.

The device has to have the "Auto" mode set at the "Record Menu",
meaning that it will automatically start to record when it detects
inserted finger and stop when the finger is out. This is the
operational mode of the device for which the software is developed.

Specify device file, using the HIDRAW driver, as an argument to the
CLI program and it will communicate with the device to synchronize
date and time of the PC to the device and download from it all the
stored records to the working directory in manufacturer's CSV and SpO2
(binary) formats. Filenames will have the date and time of the start
of a record and the extensions of the formats.

The documentation is based on the observation of the device
communication with its manufacturer's software. The code is based on
the insights, obtained from the observation, and written to liberate
the user of the device from the need to use the proprietary software
for downloading records.

The documentation is technical; supplements the code and vice versa.

The code is written in C99 with few GNU extensions (time and
endianness). It may be used standalone, but would be much more useful
in sleep tracking and analysis free software, like [OSCAR].

[OSCAR]: https://www.sleepfiles.com/OSCAR/

# How Manufacturer's and this PC Software Differ

## Manufacturer's Software

Source code is not available (proprietary).

Host OS: MS-Windows.

Device manufacturer's PC software:

`Smart Device Assistant V3.1.0.1 Setup.exe`

Its SHA1:

`ee579cbb93bf42a8f8e3891e988aed35ab18643e`

Saves files in dedicated, top directory. File names have the format:

`_<user name>_<user index>_<record index>_<file's save timestamp>.{extension}`

`{extension}` may be one of `{csv|{SpO2|part}|txt}`. The number of
files saved per a record variate.

Always deletes files from the device when they are downloaded.

## This Software

This is free software (GPLv3+).

Host OS: any, which can build the binaries and run them.

Saves files in the working directory. File names have the format:

`<record's save timestamp>.{csv|SpO2}`

Decoded data points are saved in manufacturer's CSV and SpO2 (binary)
formats; 2 files per record.

Can leave the downloaded files on the device (they can be downloaded
again), but by default deletes them.

# Communication Protocol Parts and Data Interpretation

## Overview

The device is HID, with which the communication happens employing USB
(Bluetooth could work too), through HIDRAW driver, with 64 B HID
reports, both ways. The reports are not numbered.

Bs (from hereon, and in code comments, "Bs" is the abbreviation of
"bytes", for brevity) in the reports have manufacturer's commands with
their data. The commands variate in length, but always end with a
single checksum B, which is a sum of all the previous Bs of a command
to a 7 bits B.

The device provides the metadata about records stored on it, which
includes the date and time when a record was started, the number of
measurement data points (from hereon -- datums) of the record -- the
length of the record.

Maximum length of a record is 24 h of s. That number, however, does
not include the number of adjustment Bs, which depends on the dynamics
of a measurement. The adjustment Bs may be anywhere inside datums and
their number cannot be known in advance.

The datums of a record are sent in batches. First, the SpO2 is sent
and then the PR.

2 datums of a measurement are encoded in a single B.

## Datums and Interpretation

### Datums Command

Measurements containing command (from hereon -- datums command) is 30
Bs in size. Datums in it start from the 9th B and end at the 29th
B. In the datum Bs, there may be 2 consecutive adjustment Bs; they may
cross the packet boundary. The first Bs of datums command start with
those adjustment Bs.

The 2 adjustment Bs are used to set a current, top, decoded datum of a
measurement, from which encoded datum nibble values are to be
subtracted to decode the next datum. Datums are encoded in high and
low nibbles of a datums B. 4 bits per nibble.

0x7f B in datums is padding, which can be ignored, since datum left
equaling to 0 indicates to not read another report. It is assumed that
if the last datum is in the last B's low nibble (before checksum B) in
the report, new datums command to indicate that there are no more
datum is not sent to PC (a few cases are seen where the single padding
B of 0x7f is right before checksum and no new datums command of 30 Bs
is written; all the rest Bs are 0).

### Datums B Interpretation

Datums command has 3 flag Bs. 7 least significant bits of each are the
flags for respective datums Bs. A datums B packs 2 encoded datums. For
the datums B, the flag set for it indicates whether singleton, duple
or adjustment interpretation should be done:

1. *Singleton*. Range 0x[0..7]. Bs with them have flag unset. All
other Bs have flags set.

2. *Duple*. Range 0x[0..6] and 0x[8..e]. High nibble is always lower
than the low nibble. To decode high nibble datum, subtract 0x8 more.

3. *Adjustment*. Always 2 consecutive Bs and have high nibbles 0x7
(duple has no 0x7 as a high nibble value). First adjustment B's low
nibble is the multiplier to 16. Second adjustment B's low nibble is an
addition to the result of the previous multiplication.

For singleton and duple, low nibble may be 0xf, which indicates the
value to drop and is followed by 2 adjustment Bs (if not already in
adjustment or no more datum).

## Individual Records

Even though manufacturer software's PC GUI allows to select individual
records, but the individually selected records (if not all) cannot be
downloaded, nor they can be deleted. By the GUI software, all records
are downloaded at the same time and deleted.

# Testing

## Who should Perform

Testing is meant for development. If just everyday usage is cared
about, please read the **For Everyday** and the **Usage**.

## Macros Used

To test the software executables, these compiler preprocessor
arguments to the `-D` option are used:

* `SIMULATOR` -- use test device, i.e. all software ran locally.

* `DEBUG` -- print general debugging output.

* `DEBUG_WRITE` -- monitor HID reports of exchanges of the software
with the real device (except the download of datums commands, that
could be voluminous; other tool is to be used for those). When testing
with the simulator, printing written reports duplicates read reports.

## For Everyday

To build and install the executable for everyday usage of downloading
measurement records, stored on device, at the installation directory
(below has an example directory, which must exist, within the normal
user's home directory):

`$ make && make DESTDIR=~/pulseoxdl-exec install`

If the last printed line is the "done", the `pulseoxdl` executable is
ready for everyday usage.

## Automation

### Fully Automated

Fully automated test is with the simulator, i.e. no real device is
needed. The test is performed with real measurements, with over 10000
datums. To build executables and run the test on them:

`$ make testlocal`

If the last printed line is the "pass", the executables building and
foreseen tests ran successfully.

### Partially Automated

Testing with the real device involves manual work, which has to be
done for records download from the device and reviewing detailed
program output to console. To build the executable:

`$ make testdevice`

# Usage

Plug in the device to PC and turn it on.

Find out device ID:

`$ lsusb`

Find out its HIDRAW device file:

`# echo /dev/$(dmesg | grep -i <device ID> | grep -oP 'hidraw\d+')`

Make normal user to be able to communicate with the device:

`# setfacl -m u:<normal user>:rw /dev/hidraw<N>`

Synchronize PC time to device and *move* its records to the PC:

`$ ./pulseoxdl /dev/hidraw<N>`

Synchronize PC time to device and *copy* its records to the PC:

`$ ./pulseoxdl /dev/hidraw<N> copy`

The downloaded records will be in the directory from where the
executable was called. If the same records are downloaded again, they
will overwrite the previously downloaded records with the same
filenames and their contents.

# Copyright

Copyright © 2021 Donatas Klimašauskas

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

Full license is at the COPYING file.

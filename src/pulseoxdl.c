/* pulseoxdl - pulse oximetry downloader (Contec CMS50E, USB HID)
 * Copyright © 2021 Donatas Klimašauskas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <endian.h>

#include "exchange.h"

#define CLI_ARG_HELP "-h"
#define MULTIPLICAND 16 /* Adjustment's first B's low nibble -- multiplier. */
#define LEN_CMD_PKT 30 /* Datums command packet (of a measurement). */
#define LEN_CMD_DATUMS 29 /* Datums command packet without checksum B. */
#define DATUMS_OFFSET 8 /* Datums (encoded) start at the B offset. */
#define LEN_DATUMS 21 /* Datums part. */
#define SEQUENCE_MAX 16383 /* 0x7f + (0x7f << 7). */
#define LEN_MAX_RECORDS 99 /* Manufacturer specified maximum stored records. */
#define LEN_START_TIME 20 /* E.g., "2021-01-23T12:34:56". */
#define LEN_ORIG_TIME 21 /* E.g., "2021-01-23, 12:34:56". */
#define LEN_DURATION 9 /* E.g., "12:34:56". */
#define INT32_SIZE 4 /* In Bs. */
#define RECORD_INDEX 4 /* There record's index -- its number, is set. */
#define LEN_BIN_MIDDLE 28 /* 7 * 4 LSB Bs: time parts and record length. */
#define LEN_TIME_FILENAME 20 /* E.g., "20210123123456.SpO2". */
#define TIME_DELIM "-T:" /* ISO 8601 format's delimiters, as used. */
#define CSV_LINE_TERMINATOR "\r\n"
#define CSV_HEADER "DATE,TIME,SPO2,PULSE"
#define DELETED_2ND_CHECKSUM 6

enum measurements {
	MEASUREMENT_SPO2 = 1,
	MEASUREMENT_PR,
};

static const struct fmt_time {
	const unsigned char size;
	const char *format; /* strftime(3) format string. */
} duration = {
	LEN_DURATION,
	"%T",
}, recstarttime = {
	LEN_START_TIME,
	"%FT%T",
}, origtime = {
	LEN_ORIG_TIME,
	"%F, %T",
};
struct starttime {
	time_t sec; /* Seconds since the Epoch. */
	char str[LEN_START_TIME];
};
static struct metadata {
	unsigned char rindex; /* Record's index. */
	time_t length; /* Datums (and record's duration in s). */
	char duration[LEN_DURATION];
	struct starttime starttime;
} records[LEN_MAX_RECORDS];

extern const unsigned char _binary_data_header_start;
extern const unsigned char _binary_data_header_end;

#ifndef SIMULATOR
static FILE *dev;
#endif

static unsigned char cmdpkt[LEN_CMD_PKT];
static unsigned char binmiddle[LEN_BIN_MIDDLE];
static unsigned char *printstore; /* Points to memory for a measurement. */
static unsigned char *printstorespo2; /* Measurement's memory. */
static unsigned char *printstorepr; /* Measurement's memory. */
static unsigned char top;
static unsigned int datums;

static void
store_datum(const unsigned char nibble)
{
	printstore[--datums] = top - nibble;
}

static void
process_nibbles(const unsigned char nibbles, const unsigned char flag)
{
	unsigned char high = nibbles >> 4;
	unsigned char low = nibbles & 0xf;
	static unsigned char first = 1;

	/* Adjust. */
	if (flag && high == 0x7) {
		if (first) {
			first = 0;
			top = MULTIPLICAND * low;
		} else {
			first = 1;
			top += low;
		}

		return;
	}

	/* Decode. */
	if (flag)
		store_datum(high + 0x8);
	else
		store_datum(high);
	if (low != 0xf)
		store_datum(low);
}

static void
set_current_time(unsigned char *buf)
{
	const time_t now = time(NULL);
	const struct tm *tm = localtime(&now);

	if (!tm)
		exit_error("localtime");
	buf[1] = (unsigned char) tm->tm_year - 100;
	buf[2] = (unsigned char) tm->tm_mon + 1;
	buf[3] = (unsigned char) tm->tm_mday;
	buf[4] = (unsigned char) tm->tm_hour;
	buf[5] = (unsigned char) tm->tm_min;
	buf[6] = (unsigned char) tm->tm_sec;
}

static void
set_start_seconds(time_t *sec)
{
	struct tm tm;

	tzset(); /* Do not depend on the order of time function calls. */
	tm.tm_year	= in[4] + 100;
	tm.tm_mon	= in[5] - 1;
	tm.tm_mday	= in[6];
	tm.tm_hour	= in[7] + daylight;
	tm.tm_min	= in[8];
	tm.tm_sec	= in[9] - timezone;
	if ((*sec = mktime(&tm)) == -1)
		exit_error("mktime");
}

static void
format_time(char *buf, const time_t len, const struct fmt_time *fmt)
{
	const time_t *length = &len;
	struct tm *tm;

	if (!(tm = gmtime(length)))
		exit_error("gmtime");
	if (!strftime(buf, fmt->size, fmt->format, tm))
		exit_error("could not format time");
}

/* Get and save how many records are stored on device, if any. */
static unsigned char
set_record_metadata(const unsigned int i)
{
	if (i == LEN_MAX_RECORDS)
		exit_error("more than maximum records");

#ifdef DEBUG
	debug_exchange(record);
#endif
#ifdef SIMULATOR
	write_data(stdout, record.write);
	read_data(stdin, record.read);
#else
	write_data(dev, record.write);
	read_data(dev, record.read);
#endif

	/* Record's maximum length: 24 h. In s: 86400. On device, the
	 * length's int is encoded in 3 7-bit Bs. LSB order. B's MSb
	 * is not used and appears to always be 0. */

	records[i].rindex = in[3];
	records[i].length = in[10] + (in[11] << 7) + (in[12] << 14);
	format_time(records[i].duration, records[i].length, &duration);
	set_start_seconds(&records[i].starttime.sec);
	format_time(records[i].starttime.str, records[i].starttime.sec,
		    &recstarttime);
#ifdef DEBUG
	fprintf(stderr,
		"Index: %d\nStart time: %s\nDuration: %s (%ld datums)\n",
		in[3], records[i].starttime.str, records[i].duration,
		records[i].length);
#endif

	return !in[1];
}

/* The function requests, downloads and decodes datums. The approach
 * employs 2 buffers of 64 Bs and 30 Bs. First has the HID report,
 * second -- device measurement datums command, which is parsed and
 * driven by datums left. Otherwise it would not be clear whether 0x7f
 * could be unique indicator for no more datum; test data has it as
 * the potential indication, but then the maximum measurement for PR
 * would be 239, which would negate the manufacturer's claim of 250
 * BPM. And there are cases where only single 0x7f, as the last datums
 * B (before checksum), is present. Thus, 0x7f cannot be used to slurp
 * all data and then parse the nibbles for adjustments and datums.
 * (With the case of single 0x7f and checksum at the report boundary,
 * it could mean first B of adjustment, but reading for another report
 * would block forever.) */
static void
extract_datums(const unsigned char rindex, const unsigned char measurement)
{
	requestdata.data[RECORD_INDEX] = rindex;
	requestdata.data[MEASUREMENT_INDEX] = measurement;
#ifdef SIMULATOR
	write_data(stdout, requestdata);
#else
	write_data(dev, requestdata);
#endif

	/* Allocate memory for decoded datums. Caller frees. */
	if (!(printstore = calloc(datums, 1)))
		exit_error("calloc");

	/* Decode datums from the nibbles and print them. Ensure that
	 * datums command packets are in a sequence. Sequence is
	 * encoded in 2 Bs. LSB order. First packet is #0. As with the
	 * record length, the Bs count to 0x7f. */

	unsigned char i, j;
	unsigned char next = 0, left = 0, less = 0, flag = 0;
	unsigned char flags[LEN_DATUMS];
	unsigned short sequence = 0;
	unsigned int reset = datums;

	while (1) {
#ifdef SIMULATOR
		read_report(stdin);
#else
		read_report(dev);
#endif
		while (next + LEN_CMD_PKT < LEN_HID_REPORT && datums) {
			/* Copy datums command packet from HID report. */
			if (left) {
				memcpy(cmdpkt + left, in, less);
				next = less;
				left = 0;
			} else {
				memcpy(cmdpkt, in + next, LEN_CMD_PKT);
				next += LEN_CMD_PKT;
			}

			checksum(CHECKSUM_CHECK, cmdpkt, LEN_CMD_DATUMS);

			/* Validate in sequence. */
			if (sequence++ != cmdpkt[3] + (cmdpkt[4] << 7))
				exit_error("out of sequence");
			if (sequence > SEQUENCE_MAX)
				exit_error("sequence would overflow");

			/* Flag Bs are +5..+7. */
			for (i = 0, j = 5; i < LEN_DATUMS; i++) {
				if (!(i % 7))
					flag = cmdpkt[j++];
				flags[i] = flag & 1;
				flag >>= 1;
			}

			/* Decode datums. */
			for (i = DATUMS_OFFSET, j = 0; i < LEN_CMD_DATUMS &&
				     datums; i++, j++)
				process_nibbles(cmdpkt[i], flags[j]);
		}
		if (!datums)
			break;
		if ((left = LEN_HID_REPORT - next)) { /* In old HID report. */
			memcpy(cmdpkt, in + next, left);
			less = LEN_CMD_PKT - left; /* In new HID report. */
		}
		next = 0;
	}
	datums = reset;

	/* Assign decoded datums to their print store. */
	switch (measurement) {
	case MEASUREMENT_SPO2:
		printstorespo2 = printstore;
		break;
	case MEASUREMENT_PR:
		printstorepr = printstore;
		break;
	}
}

static void
set_binary_middle(const int data, const unsigned int offset)
{
	u_int32_t origendian = htole32(data);

	memcpy(binmiddle + offset * INT32_SIZE, &origendian, INT32_SIZE);
}

static void
write_binary(FILE *bin, const void *buf, const size_t size)
{
	if (fwrite(buf, 1, size, bin) != size)
		exit_error("writing SpO2 file failed");
}

static void
clear_filename_buffer(char *buf)
{
	memset(buf, 0, LEN_TIME_FILENAME);
}

static void
write_decoded_files(struct starttime starttime)
{
	/* Strip non-digits from ISO 8601 date and time for a filename. */
	char timecsv[LEN_TIME_FILENAME], timebin[LEN_TIME_FILENAME];
	char *part;
	unsigned int i;

	clear_filename_buffer(timecsv);
	clear_filename_buffer(timebin);
	strcat(timecsv, strtok(starttime.str, TIME_DELIM));
	set_binary_middle(atoi(timecsv), 0);
	for (i = 1; (part = strtok(NULL, TIME_DELIM)); i++) {
		strcat(timecsv, part);
		set_binary_middle(atoi(part), i);
	}
	set_binary_middle(datums, i);

	/* Write files of decoded datums in original CSV and SpO2
	 * (binary) formats. The binary is composed from header --
	 * binary data, which is, for CMS50E, repeated in every file,
	 * saved by the manufacturer's software, middle -- 7 LSB, 4 Bs
	 * long integers of 6 parts of date and time and record's
	 * length, and bottom -- the datums. */
	FILE *csv, *bin;
	char otm[LEN_ORIG_TIME];
	unsigned int binsize = &_binary_data_header_end -
		&_binary_data_header_start;

	strcat(timebin, timecsv);
	strcat(timecsv, ".csv");
	strcat(timebin, ".SpO2");
	open_file(&csv, timecsv, "w");
	open_file(&bin, timebin, "wb");
	fprintf(csv, "%s"CSV_LINE_TERMINATOR, CSV_HEADER);
	write_binary(bin, &_binary_data_header_start, binsize);
	write_binary(bin, binmiddle, LEN_BIN_MIDDLE);
	for (i = 0; datums--; i++) {
		format_time(otm, starttime.sec + i, &origtime);
		fprintf(csv, "%s, %d, %d"CSV_LINE_TERMINATOR, otm,
			printstorespo2[datums], printstorepr[datums]);
		putc(printstorespo2[datums], bin);
		putc(printstorepr[datums], bin);
	}
	close_file(csv);
	close_file(bin);
}

static void
exit_help(void)
{
	printf("%s: usage: ./%s /dev/hidraw<N> [copy]\n", program, program);

	exit(EXIT_SUCCESS);
}

int
main(int argc, char *argv[])
{
	set_program_name(argv[0]);

	if (argc < 2)
		exit_error("device file is required; try just "CLI_ARG_HELP);
	if (!strncmp(argv[1], CLI_ARG_HELP, CLI_ARG_MAX_LEN))
		exit_help();

	/* Initial exchange. */

	unsigned int i;
	unsigned char nostoredauto = 1;

#ifdef SIMULATOR
	unbuf_stdout();
#else
	open_file(&dev, argv[1], "r+b");
#endif
	for (i = 0; i < LEN_EXCHANGES; i++) {
#ifdef DEBUG
		debug_exchange(exchanges[i]);
#endif
		if (exchanges[i].id == SYNCHRONIZE_DEVICE_DATE_AND_TIME)
			set_current_time(exchanges[i].write.data);
#ifdef SIMULATOR
		write_data(stdout, exchanges[i].write);
		read_data(stdin, exchanges[i].read);
#else
		write_data(dev, exchanges[i].write);
		read_data(dev, exchanges[i].read);
#endif
		if (exchanges[i].id == STORED_DATA_IN_MODE)
			if (in[2] && in[3])
				nostoredauto = 0;
	}

	if (nostoredauto)
		exit_error("no Auto mode data stored");

	/* Download and save all metadata about records stored on device. */
	for (i = 0; set_record_metadata(i); i++)
		;

	/* Read all records and their datums, decode, write files. */
	for (i = 0; records[i].rindex; i++) {
		datums = records[i].length;
		extract_datums(records[i].rindex, MEASUREMENT_SPO2);
		extract_datums(records[i].rindex, MEASUREMENT_PR);
		write_decoded_files(records[i].starttime);
		free(printstorespo2);
		free(printstorepr);
	}

	/* Remove the records from device, unless user requested copying. */
	if (argc < 3 || strncmp(argv[2], "copy", CLI_ARG_MAX_LEN)) {
#ifdef DEBUG
		debug_exchange(delete);
#endif
#ifdef SIMULATOR
		write_data(stdout, delete.write);
		read_data(stdin, delete.read);
#else
		write_data(dev, delete.write);
		read_data(dev, delete.read);
#endif
		checksum(CHECKSUM_CHECK, in + delete.read.checksum,
			 DELETED_2ND_CHECKSUM);
	}

#ifndef SIMULATOR
	close_file(dev);
#endif

	exit(EXIT_SUCCESS);
}

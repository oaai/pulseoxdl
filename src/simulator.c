/* pulseoxdl - pulse oximetry downloader (Contec CMS50E, USB HID)
 * Copyright © 2021 Donatas Klimašauskas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>

#include "exchange.h"

void
respond_data(char *argv[])
{
	read_data(stdin, requestdata);

	FILE *fp;

	open_file(&fp, argv[in[MEASUREMENT_INDEX]], "rb");
	while (1) {
		if (fread(in, 1, LEN_HID_REPORT, fp) != LEN_HID_REPORT)
			if (feof(fp))
				break;
		exit_on_read_error(fp);
		if (fwrite(in, 1, LEN_HID_REPORT, stdout) != LEN_HID_REPORT)
			exit_error("writing to stdout failed");
	}
	close_file(fp);
}

int
main(int argc, char *argv[])
{
	set_program_name(argv[0]);

	if (argc < 3)
		exit_error("SpO2 and PR transmission files are required");

	unsigned int i;

	unbuf_stdout();
	for (i = 0; i < LEN_EXCHANGES; i++) {
#ifdef DEBUG
		debug_exchange(exchanges[i]);
#endif
		read_data(stdin, exchanges[i].write);
		write_data(stdout, exchanges[i].read);
	}

#ifdef DEBUG
	debug_exchange(record);
#endif
	read_data(stdin, record.write);
	write_data(stdout, record.read);

	respond_data(argv); /* SpO2 expected, but could be PR. */
	respond_data(argv); /* PR expected, but could be SpO2. */

#ifdef DEBUG
	debug_exchange(delete);
#endif
	read_data(stdin, delete.write);
	write_data(stdout, delete.read);

	exit(EXIT_SUCCESS);
}
